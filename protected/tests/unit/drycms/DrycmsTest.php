<?php
class DrycmsTest extends CDbTestCase
{
	public $fixtures = array(
		'pages' => 'Page',
	);
	
	public function testPagesTreeIsArray()
	{
		$cms = Yii::app()->cms;
		
		$this->assertTrue(is_array($cms->pagesTree));
	}
	
	public function testPagesTreeRootCount()
	{
		$cms = Yii::app()->cms;
		
		// Количество элементов с prent_id = 0
		// активных и неудаленых
		$c = 0;
		foreach ($this->pages as $p) {
			if ($p['parent_id'] == 0 &&
			$p['active'] == 1 && $p['deleted'] == 0)
				$c++;
		}
		
		$this->assertEquals($c, count($cms->pagesTree));
	}
	
	public function testFindPage()
	{
		$cms = Yii::app()->cms;
		
		$url = '/about';
		$currentPage = $cms->findPage($url);
		$page = Page::model()->findByPk(3);
		$this->assertEquals($page->id, $currentPage->id);
		
		$url = '/about/personal';
		$currentPage = $cms->findPage($url);
		$page = Page::model()->findByPk(5);
		$this->assertEquals($page->id, $currentPage->id);
		
		$url = '/';
		$currentPage = $cms->findPage($url);
		$page = Page::model()->findByPk(1);
		$this->assertEquals($page->id, $currentPage->id);
		
		
	}
}