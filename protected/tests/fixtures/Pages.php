<?php
return array(
	'page1' => array(
		'title' => 'Главная страница',
		'body' => 'Текст страницы',
		'parent_id' => 0,
		'url' => '',
		'controller' => 'static',
		'deleted' => 0,
		'active' => 1,
		'position' => 2,
	),
	
	'page2' => array(
		'title' => 'Контакты',
		'body' => 'Текст страницы',
		'parent_id' => 0,
		'url' => 'contacts',
		'controller' => 'static',
		'deleted' => 0,
		'active' => 1,
		'position' => 4,
	),
	'page3' => array(
		'title' => 'О компании',
		'body' => 'Текст страницы',
		'parent_id' => 0,
		'url' => 'about',
		'controller' => 'static',
		'deleted' => 0,
		'active' => 1,
		'position' => 6,
	),
	'page4' => array(
		'title' => 'История',
		'body' => 'Текст страницы',
		'parent_id' => 3,
		'url' => 'history',
		'controller' => 'static',
		'deleted' => 1,
		'active' => 1,
		'position' => 4,
	),
	'page5' => array(
		'title' => 'Сотрудники',
		'body' => 'Текст страницы',
		'parent_id' => 3,
		'url' => 'personal',
		'controller' => 'static',
		'deleted' => 0,
		'active' => 1,
		'position' => 2,
	),
	'page6' => array(
		'title' => 'Сертификаты',
		'body' => 'Текст страницы',
		'parent_id' => 3,
		'url' => 'cert',
		'controller' => 'static',
		'deleted' => 1,
		'active' => 1,
		'position' => 6,
	),
	'page7' => array(
		'title' => 'Каталог',
		'body' => 'Текст страницы',
		'parent_id' => 0,
		'url' => 'catalog',
		'controller' => 'static',
		'deleted' => 0,
		'active' => 0,
		'position' => 7,
	),
	'page8' => array(
		'title' => 'Статьи',
		'body' => 'Текст страницы',
		'parent_id' => 0,
		'url' => 'articles',
		'controller' => 'article',
		'deleted' => 0,
		'active' => 0,
		'position' => 10,
	),
	'page9' => array(
		'title' => 'Сертификаты',
		'body' => 'Текст страницы',
		'parent_id' => 5,
		'url' => 'cert',
		'controller' => 'static',
		'deleted' => 0,
		'active' => 1,
		'position' => 2,
	),
);