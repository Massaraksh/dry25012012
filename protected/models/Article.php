<?php

/**
 * This is the model class for table "articles".
 *
 * The followings are the available columns in table 'articles':
 * @property integer $id
 * @property string $title
 * @property string $body
 * @property string $date
 * @property string $created
 * @property integer $deleted
 * @property integer $active
 */
class Article extends CActiveRecord
{
	public function behaviors()
	{
		return array(
			'deleted' => array(
				'class' => 'application.modules.drycms.components.behaviors.DeletedBehavior',
			),
			'created' => array(
				'class' => 'application.modules.drycms.components.behaviors.CreatedBehavior',
			),
			'active' => array(
				'class' => 'application.modules.drycms.components.behaviors.ActiveBehavior',
			),
		);
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * @return Article the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'articles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date', 'required'),
			array('deleted, active', 'numerical', 'integerOnly'=>true),
			array('title, body, created', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, body, date, created, deleted, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Заголовок',
			'body' => 'Содержание',
			'date' => 'Дата',
			'created' => 'Created',
			'deleted' => 'Deleted',
			'active' => 'Активна ?',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('deleted',$this->deleted);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function fields()
	{
		return array(
			'title' => array('type' => 'textbox', 'table'),
			'body' => array('type' => 'textarea', 'table'),
			'date' => array('type' => 'date', 'table'),
                        'active' => array('type' => 'textbox', 'table')
		);
	}
}