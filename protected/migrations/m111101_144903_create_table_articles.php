<?php

class m111101_144903_create_table_articles extends CDbMigration
{
	public function up()
	{
		$this->createTable('articles', array(
			'id' => 'pk',
			'title' => 'text',
			'announce' => 'text',
			'body' => 'text',
			'date' => 'date',
			'active' => 'boolean NOT NULL DEFAULT 1',
			'deleted' => 'boolean NOT NULL DEFAULT 0',
			'created' => 'timestamp DEFAULT CURRENT_TIMESTAMP',
		));
	}

	public function down()
	{
		$this->dropTable('articles');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
