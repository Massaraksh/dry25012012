<?php

class m111120_125650_alter_table_pages extends CDbMigration
{
	public function up()
	{
		$this->addColumn('pages', 'position', 'integer NOT NULL DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('pages', 'position');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}