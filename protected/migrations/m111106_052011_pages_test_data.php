<?php

class m111106_052011_pages_test_data extends CDbMigration
{
	public function up()
	{
		$this->insert('pages', array(
			'title' => 'Главная страница',
			'parent_id' => 0,
			'body' => 'Админка временно тут <a href="/dryadmin/">/dryadmin/</a>',
			'url' => '',
			'controller' => 'static',
			'deleted' => 0,
			'active' => '1'
		));

	}

	public function down()
	{
		
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
