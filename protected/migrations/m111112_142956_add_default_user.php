<?php

class m111112_142956_add_default_user extends CDbMigration
{
	public function up()
	{
		$this->insert('users', array(
			'name' => 'Администратор',
			'email' => 'admin@example.com',
			'login' => 'admin',
			'password' => sha1('password'),
			'active' => 1,
			'deleted' => 0
		));
	}

	public function down()
	{
		
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}