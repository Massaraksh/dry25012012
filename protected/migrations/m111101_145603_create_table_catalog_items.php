<?php

class m111101_145603_create_table_catalog_items extends CDbMigration
{
	public function up()
	{
		$this->createTable('catalog_items', array(
			'id' => 'pk',
			'catalog_id' => 'integer NOT NULL DEFAULT 0',
			'title' => 'text',
			'text' => 'text',
			'price' => 'float',
			'active' => 'boolean NOT NULL DEFAULT 1',
			'deleted' => 'boolean NOT NULL DEFAULT 0',
			'created' => 'timestamp DEFAULT CURRENT_TIMESTAMP',
		));
	}

	public function down()
	{
		$this->dropTable('catalog_items');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
