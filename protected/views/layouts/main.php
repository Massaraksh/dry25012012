<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->assetManager->publish(Yii::getPathOfAlias('application.modules.dryadmin.assets')); ?>/twitter-bootstrap/bootstrap.min.css" />
	<title><?php echo CHtml::encode(Yii::app()->cms->currentPage->title); ?></title>
</head>

<body>

<div class="container">

	
	<div id="header" class="page-header">
		<h1> <a href="<?php echo $this->createUrl('');?>"> <?php echo CHtml::encode(Yii::app()->name); ?></a></h1>
	</div><!-- header -->
	
	<div class="row">
		<div id="mainmenu" class="span4">
			<?php $this->widget('zii.widgets.CMenu',array(
				'items'=>Yii::app()->cms->menu
				)
			); ?>
		</div><!-- mainmenu -->
		
			
		<div class="span12">
			<?php if(isset($this->breadcrumbs)):?>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
				)); ?><!-- breadcrumbs -->
			<?php endif?>
			<?php echo $content; ?>
		</div>
	</div>
	
	<div id="footer" class="footer">
		Copyright &copy; <?php echo date('Y'); ?> Dry pre 1<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->
	
</div><!-- page -->

</body>
</html>