<?php
class DryCMS extends CApplicationComponent
{
	/**
	 * Дерево со страницами
	 * @var array
	 */
	protected $_pagesTree = NULL;
	
	/**
	 * Текущая запрошенная страницы
	 * @var Page
	 */
	protected $_currentPage = NULL;
	
	/**
	 * Меню
	 * Массив в формате совместимом с CMenu
	 * @var array
	 */
	protected $_menu;
	
	public function init()
	{
		return parent::init();
	}
	
	/**
	 * Getter для дерева страниц
	 */
	public function getPagesTree()
	{
		if ($this->_pagesTree === NULL) {
			$this->_pagesTree = Page::model()->root()->notRemoved()->active()->sorted()->findAll();
		}
		
		return $this->_pagesTree;
	}
	
	public function getCurrentPage()
	{
		if ($this->_currentPage === NULL) {
			$this->_currentPage = $this->findPage(Yii::app()->request->pathInfo);
		}
		
		return $this->_currentPage;
	}
	
	public function findPage($url)
	{
		$urlTokens = explode('/', trim($url, '/'));
		
		$branch = $this->getPagesTree();
		$found = TRUE;
		while ($found) {
			$found = FALSE;
			if (is_array($branch)){
				foreach ($branch as $p) {
					if ($p->url === $urlTokens[0]) {
						$found = TRUE;
						$page = $p;
						$branch = $page->children;
						array_shift($urlTokens);
						break;
					}
				}
			}
		}
		//CVarDumper::dump($page);
		
		return $page;
	}

	public function processRequest()
	{
		// Находим остаток URL
		$restUrl = mb_ereg_replace(trim($this->getCurrentPage()->fullUrl, '/'), '', Yii::app()->request->pathInfo);
		
		// Строим новый url
		$newUrl = $this->getCurrentPage()->controller.$restUrl;
		
		/*
		 * пропустить $newUrl через urlManager чтобы он нашел там,
		 * соответсвующие роутеры и пропарсил параметры передаваемые в URL.
		 */
		$route = Yii::app()->urlManager->parseUrl(Yii::app()->request);
		
		$route = $newUrl;
		
		Yii::app()->runController($route);
	}
	
	/**
	 * Getter для $this->menu
	 */
	public function getMenu()
	{
		if ($this->_menu === NULL)
		{
			
			$this->_menu = $this->buildMenu($this->pagesTree);
		}
		
		return $this->_menu;
	}
	
	/**
	 * Сборка меню
	 * @param array $pages
	 */
	public function buildMenu($pages)
	{
		$items = array();
		foreach ($pages as $page)
		{
			$item = array('label' => $page->title, 'url' => $page->fullUrl);
			
			if ($page->children) $item['items'] = $this->buildMenu($page->children);
			
			$items[] = $item;
		}
		return $items;
	}
}