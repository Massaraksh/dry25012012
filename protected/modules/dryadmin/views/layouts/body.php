<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

		<?php Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/twitter-bootstrap/bootstrap.css'); ?>
	
	<?php echo CHtml::cssFile($this->module->assetsUrl.'/jqui/css/Aristo/Aristo.css')?>
	<?php echo CHtml::cssFile($this->module->assetsUrl.'/elfinder/css/elfinder.css')?>

	<?php echo CHtml::scriptFile($this->module->assetsUrl.'/jquery-1.7.min.js'); ?>
	<?php echo CHtml::scriptFile($this->module->assetsUrl.'/jqui/js/jquery-ui-1.8.16.custom.min.js'); ?>
	<?php echo CHtml::scriptFile($this->module->assetsUrl.'/jqui/js/i18n/jquery.ui.datepicker-ru.js'); ?>
	
	<?php echo CHtml::scriptFile($this->module->assetsUrl.'/ckeditor/ckeditor.js'); ?>
	<?php echo CHtml::scriptFile($this->module->assetsUrl.'/ckeditor/adapters/jquery.js'); ?>
	
	<?php echo CHtml::scriptFile($this->module->assetsUrl.'/elfinder/js/elfinder.min.js'); ?>
	<?php echo CHtml::scriptFile($this->module->assetsUrl.'/elfinder/js/i18n/elfinder.ru.js'); ?>
	
	
	<?php echo CHtml::script("$.datepicker.setDefaults($.datepicker.regional['ru']);")?>
	
	
	
	<?php Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/iconic/iconic.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/admin-style.css'); ?>
	

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
	<?php echo $content; ?>
</body>
</html>