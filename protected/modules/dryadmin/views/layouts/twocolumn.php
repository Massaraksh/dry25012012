<?php $this->beginContent('/layouts/body'); ?>
<div class="topbar">
	<div class="topbar-inner">
		<div class="container-fluid">
			<a class="brand" href="<?php echo $this->createUrl('section/index', array('section' => 'page')); ?>">DryAdmin</a>
                        <a class="brand" href="<?php echo Yii::app()->homeurl; ?>">DryCMS</a>
                        <a class="brand" href="<?php //echo Yii::app()->user->logout(); ?>">Выход</a>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="sidebar">
		<div class="well">
			<ul>
				<?php foreach ($this->module->structure as $sectionId => $section):?>
					<?php if (!isset($section['master'])): ?>
					<li><a href="<?php echo $this->createUrl('section/index', array('section' => $sectionId)); ?>"><?php echo $section['label']; ?></a></li>
					<?php endif; ?>
				<?php endforeach;?>
                                <li><a href="<?php echo Yii::app()->homeurl . 'dryadmin/user'; ?>">Пользователи</a></li>
                                
			</ul>
		</div>
	</div>
	<div class="content">
		<?php $this->widget('DBreadcrumbs', array('links' => $this->breadcrumbs, 'homeLink' => FALSE)); ?>
		<?php echo $content; ?>
                	<div class="span-5 last">
                            <div id="sidebar">
                            <?php
                                    $this->beginWidget('zii.widgets.CPortlet', array(
                                            'title'=>'<br>Операции',
                                    ));
                                    $this->widget('zii.widgets.CMenu', array(
                                            'items'=>$this->menu,
                                            'htmlOptions'=>array('class'=>'operations'),
                                    ));
                                    $this->endWidget();
                            ?>
                            </div><!-- sidebar -->
                        </div>
	</div>
</div>
<?php $this->endContent();?>